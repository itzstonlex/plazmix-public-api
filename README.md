<div align="center">

# Plazmix Network :: Public API
[![MIT License](https://img.shields.io/github/license/pl3xgaming/Purpur?&logo=github)](License)

------------------------------------------
Система разработана **ItzStonlex** специально для игрового сервера Minecraft: 
  _**Plazmix Network (mc.plazmix.net)**_
</div>

------------------------------------------
### Обратная связь
* **[Discord chat](https://plazmix.xyz/discord)**
* **[ВКонтакте](https://vk.com/plazmixnetwork)**
------------------------------------------
### Получение токена авторизации
* _[Нажмите сюда](https://dev.plazmix.net/teams)_ для перехода по ссылке
------------------------------------------

## API Connection
```java
public static final PlazmixPublicApi PUBLIC_API = PlazmixPublicApi.newOAuth()
            
            .setApiVersion("[last api verison]")
            .setAuthToken("[your auth token]")

            .setDebug(true) // Allow to debugging log-messages.
            .build();
```
------------------------------------------
На данный момент доступно несколько видов rest-api:

|   Тип API      | Способ получения                             |
|----------------|----------------------------------------------|
| PartiesRestApi | `PUBLIC_API.getApiTypes().getPartiesApi()`   |
| FriendsRestApi | `PUBLIC_API.getApiTypes().getFriendsApi()`   |
| PlayerRestApi  | `PUBLIC_API.getApiTypes().getPlayersApi()`   |
| GamesRestApi   | `PUBLIC_API.getApiTypes().getGamesDataApi()` |
***

## Response Exceptions
Любой запрос на получение response данных требует обработку 
исключения `PlazmixNotFoundException`. Он выбрасывается в том случае,
если результат запроса не был найден, либо соединение с сервером не было
установлено

```java
try {
    PlayerRestApi playersApi = PUBLIC_API.getApiTypes().getPlayersApi();
    PlayerRestApi.GetPlayerInfoResult playerInfoResult = playersApi.getPlayerInfo("ItzStonlex");

    if (playerInfoResult.isSuccess()) {
        // handle response result.
    }

} catch (PlazmixNotFoundException exception) {
    System.err.println("[PlazmixAPI] :: No API Connection!");

    exception.printStackTrace();
}
```
------------------------------------------
## _API Versions_

Список поддерживаемых версий под _rest-api_:
 * **v1.0.0**
 * ...
