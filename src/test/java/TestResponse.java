import net.plazmix.api.PlazmixPublicApi;
import net.plazmix.api.exception.PlazmixNotFoundException;
import net.plazmix.api.request.PlayerRestApi;

public final class TestResponse {

    public static final PlazmixPublicApi PUBLIC_API = PlazmixPublicApi.newOAuth()
            .setApiVersion("v1.1")
            .setAuthToken("fdf-3289ffb9we-bdsu9gwf")

            .setDebug(true)
            .build();

    public static void main(String[] args) {
        try {
            PlayerRestApi playersApi = PUBLIC_API.getApiTypes().getPlayersApi();
            PlayerRestApi.GetPlayerInfoResult playerInfoResult = playersApi.getPlayerInfo("Krashe85");

            if (playerInfoResult.isSuccess()) {
                System.out.println(playerInfoResult.getId());
                System.out.println(playerInfoResult.getName());

                System.out.println(playerInfoResult.getBalance().getCoins());
                System.out.println(playerInfoResult.getBalance().getPlazma());

                System.out.println(playerInfoResult.getLeveling().getLevel());
                System.out.println(playerInfoResult.getLeveling().getExperience());
                System.out.println(playerInfoResult.getLeveling().getMaxExperience());

                System.out.println(playerInfoResult.getGroup().getLevel());
                System.out.println(playerInfoResult.getGroup().getName());
                System.out.println(playerInfoResult.getGroup().getDisplayName());
                System.out.println(playerInfoResult.getGroup().getPrefix());
                System.out.println(playerInfoResult.getGroup().getSuffix());
                System.out.println(playerInfoResult.getGroup().getTagPriority());
            }

        } catch (PlazmixNotFoundException exception) {
            System.err.println("[PlazmixAPI] :: No API Connection!");

            exception.printStackTrace();
        }
    }

}
