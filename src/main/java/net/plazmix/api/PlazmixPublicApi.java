package net.plazmix.api;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import net.plazmix.api.request.FriendsRestApi;
import net.plazmix.api.request.GamesRestApi;
import net.plazmix.api.request.PartiesRestApi;
import net.plazmix.api.request.PlayerRestApi;
import net.plazmix.api.response.PlazmixHttpResponse;
import net.plazmix.api.response.PlazmixJsonResponse;
import net.plazmix.api.util.HttpParameters;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Builder(builderClassName = "Builder", builderMethodName = "newOAuth", setterPrefix = "set")
public class PlazmixPublicApi {

    @Getter
    @FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
    public class ApiTypes {

        PartiesRestApi partiesApi   = new PartiesRestApi(PlazmixPublicApi.this);
        FriendsRestApi friendsApi   = new FriendsRestApi(PlazmixPublicApi.this);
        PlayerRestApi playersApi    = new PlayerRestApi(PlazmixPublicApi.this);
        GamesRestApi gamesDataApi   = new GamesRestApi(PlazmixPublicApi.this);
    }


// ================================================================================================================== //

    public static final String BASE_API_URL         = "https://api.plazmix.net/%s/";

    public static final Gson GSON                   = new Gson();
    public static final JsonParser JSON_PARSER      = new JsonParser();

    static {
        System.setProperty("web-server", BASE_API_URL.substring(0, BASE_API_URL.length() - 3));

        System.out.println("                                                      ");
        System.out.println("======================================================");
        System.out.println("                                                      ");
        System.out.println("          Plazmix Network ** PublicAPI                ");
        System.out.println("               Thanks for using!                      ");
        System.out.println("                                                      ");
        System.out.println("                  [VK Contact]:                       ");
        System.out.println("           https://vk.com/plazmixnetwork/             ");
        System.out.println("                                                      ");
        System.out.println("======================================================");
        System.out.println("                                                      ");
    }

// ================================================================================================================== //

    @NonNull
    boolean debug;

    @NonNull
    String apiVersion;

    @NonNull
    String authToken;


    @NonNull
    ApiTypes apiTypes = new ApiTypes();


    @NonNull
    HttpClient httpClient = HttpClientBuilder.create().setUserAgent("Plazmix PublicAPI/1.0").build();

    @NonNull
    Executor executorService = Executors.newCachedThreadPool();


    public final <R> CompletableFuture<PlazmixHttpResponse<R>> get(@NonNull Class<R> handler, @NonNull String request) {
        return get(handler, request, null);
    }

    public final <R> CompletableFuture<PlazmixHttpResponse<R>> get(@NonNull Class<R> handler, @NonNull String request,
                                                                   HttpParameters parameters) {

        String url = String.format(BASE_API_URL, apiVersion) + request;

        if (parameters != null) {
            url = parameters.apply(url);
        }

        if (isDebug()) {
            System.out.println("[PlazmixAPI] :: Handle connection to \"" + url + "\"...");
        }

        HttpGet httpGet = new HttpGet(url);

        httpGet.addHeader("Content-Type", "application/json");
        httpGet.addHeader("Authorization", "Bearer " + authToken);

        return CompletableFuture.supplyAsync(() -> {

            try {
                HttpResponse response = httpClient.execute(httpGet);

                if (isDebug()) {
                    System.out.println("[PlazmixAPI] :: Response code: " + response.getStatusLine().getStatusCode());
                }

                int responseCode = response.getStatusLine().getStatusCode();
                String responseBody = EntityUtils.toString(response.getEntity(), "UTF-8");

                return PlazmixHttpResponse.create(handler, responseCode, responseBody);
            }

            catch (IOException exception) {
                System.out.println("[PlazmixAPI] :: Connection refused:");

                throw new RuntimeException(exception);
            }

        }, this.executorService);
    }

}
