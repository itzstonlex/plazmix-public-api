package net.plazmix.api.response;

import lombok.NonNull;

public class PlazmixJsonResponse extends PlazmixHttpResponse<String> {

    public static PlazmixJsonResponse create(int responseCode, @NonNull String responseBody) {
        return new PlazmixJsonResponse(responseCode, responseBody);
    }

    
    public PlazmixJsonResponse(int responseCode, @NonNull String responseBody) {
        super(responseCode, String.class, responseBody);
    }

    @Override
    public String executeParsing() {
        return responseBody;
    }
}
