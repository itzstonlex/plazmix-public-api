package net.plazmix.api.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.*;
import lombok.experimental.FieldDefaults;
import net.plazmix.api.PlazmixPublicApi;
import net.plazmix.api.exception.PlazmixNotFoundException;

import java.net.HttpURLConnection;

@ToString
@Getter
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED, makeFinal = true)
public class PlazmixHttpResponse<T> {

    public static <T> PlazmixHttpResponse<T> create(@NonNull Class<T> requestType, int responseCode, @NonNull String responseBody) {
        return new PlazmixHttpResponse<>(responseCode, requestType, responseBody);
    }

    @NonNull int responseCode;

    @NonNull Class<T> requestType;
    @NonNull String responseBody;


    public boolean isSuccess() {
        return responseCode == HttpURLConnection.HTTP_OK && responseBody != null && requestType != null;
    }

    public T executeParsing() throws PlazmixNotFoundException {
        JsonObject jsonObject = PlazmixPublicApi.JSON_PARSER.parse(responseBody).getAsJsonObject();
        JsonElement jsonData = jsonObject.get("data");

        if (jsonData == null) {
            throw new PlazmixNotFoundException("data response");
        }

        return PlazmixPublicApi.GSON.fromJson(jsonData.toString(), requestType);
    }

}
