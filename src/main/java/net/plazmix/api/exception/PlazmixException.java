package net.plazmix.api.exception;

public class PlazmixException extends Exception {

    public PlazmixException() {
        super();
    }

    public PlazmixException(String message) {
        super(message);
    }
}
