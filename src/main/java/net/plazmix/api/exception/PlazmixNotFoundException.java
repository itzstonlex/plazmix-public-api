package net.plazmix.api.exception;

public class PlazmixNotFoundException extends PlazmixException {

    public PlazmixNotFoundException() {
        super();
    }

    public PlazmixNotFoundException(String message) {
        super(message);
    }
}
