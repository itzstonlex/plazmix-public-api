package net.plazmix.api.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import net.plazmix.api.PlazmixPublicApi;
import net.plazmix.api.exception.PlazmixNotFoundException;
import net.plazmix.api.response.PlazmixHttpResponse;
import net.plazmix.api.util.HttpParameters;

import java.util.List;
import java.util.concurrent.ExecutionException;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public final class FriendsRestApi extends PlazmixRestApi {

    public FriendsRestApi(@NonNull PlazmixPublicApi api) {
        super(api, "User.friends");
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public GetFriendsListResult getFriendsList(@NonNull String playerName) throws PlazmixNotFoundException {
        return get(GetFriendsListResult.class, HttpParameters.create().parameter("name", playerName))
                .get()
                .executeParsing();
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class GetFriendsListResult extends PlazmixHttpResponse<GetFriendsListResult> {

        String player;
        List<String> friends;

        public GetFriendsListResult(@NonNull int responseCode, @NonNull Class<GetFriendsListResult> requestType, @NonNull String responseBody) {
            super(responseCode, requestType, responseBody);
        }
    }

}
