package net.plazmix.api.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import net.plazmix.api.PlazmixPublicApi;
import net.plazmix.api.exception.PlazmixNotFoundException;
import net.plazmix.api.response.PlazmixHttpResponse;
import net.plazmix.api.util.HttpParameters;

import java.util.List;
import java.util.concurrent.ExecutionException;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public final class PartiesRestApi extends PlazmixRestApi {

    public PartiesRestApi(@NonNull PlazmixPublicApi api) {
        super(api, "User.parties");
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public GetPartyResult getParty(@NonNull String playerName) throws PlazmixNotFoundException {
        return get(GetPartyResult.class, HttpParameters.create().parameter("name", playerName))
                .get()
                .executeParsing();
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class GetPartyResult extends PlazmixHttpResponse<GetPartyResult> {

        String leader;
        List<String> members;

        public GetPartyResult(@NonNull int responseCode, @NonNull Class<GetPartyResult> requestType, @NonNull String responseBody) {
            super(responseCode, requestType, responseBody);
        }
    }

}
