package net.plazmix.api.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import net.plazmix.api.PlazmixPublicApi;
import net.plazmix.api.exception.PlazmixNotFoundException;
import net.plazmix.api.response.PlazmixHttpResponse;
import net.plazmix.api.response.PlazmixJsonResponse;
import net.plazmix.api.util.HttpParameters;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Getter
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PROTECTED)
public abstract class PlazmixRestApi {

    @NonNull PlazmixPublicApi api;
    @NonNull String request;


    public final <R> CompletableFuture<PlazmixHttpResponse<R>> get(@NonNull Class<R> handler) {
        return api.get(handler, request);
    }

    public final <R> CompletableFuture<PlazmixHttpResponse<R>> get(@NonNull Class<R> handler, HttpParameters parameters) {
        return api.get(handler, request, parameters);
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public final String getResponseJson(HttpParameters parameters) throws PlazmixNotFoundException {
        return get(PlazmixJsonResponse.class, parameters)
                .get()
                .executeParsing()
                .getResponseBody();
    }

}
