package net.plazmix.api.request;

import lombok.*;
import lombok.experimental.FieldDefaults;
import net.plazmix.api.PlazmixPublicApi;
import net.plazmix.api.exception.PlazmixNotFoundException;
import net.plazmix.api.response.PlazmixHttpResponse;
import net.plazmix.api.util.HttpParameters;

import java.util.List;
import java.util.concurrent.ExecutionException;

public final class GamesRestApi extends PlazmixRestApi {

    public GamesRestApi(@NonNull PlazmixPublicApi api) {
        super(api, "Server.games");
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public GetGameDataResult getGameData(int gameID) throws PlazmixNotFoundException {
        return get(GetGameDataResult.class, HttpParameters.create().parameter("id", gameID))
                .get()
                .executeParsing();
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class GetGameDataResult extends PlazmixHttpResponse<GetGameDataResult> {

        int id;

        long startTimeMillis;
        long endTimeMillis;

        String map;
        String winner;

        List<String> players;

        public GetGameDataResult(@NonNull int responseCode, @NonNull Class<GetGameDataResult> requestType, @NonNull String responseBody) {
            super(responseCode, requestType, responseBody);
        }
    }
}
