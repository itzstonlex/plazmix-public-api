package net.plazmix.api.request;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import net.plazmix.api.PlazmixPublicApi;
import net.plazmix.api.exception.PlazmixNotFoundException;
import net.plazmix.api.response.PlazmixHttpResponse;
import net.plazmix.api.util.HttpParameters;

import java.util.concurrent.ExecutionException;

public final class PlayerRestApi extends PlazmixRestApi {

    public PlayerRestApi(@NonNull PlazmixPublicApi api) {
        super(api, "User.info");
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public GetIdByNameResult getPlayerId(@NonNull String playerName) throws PlazmixNotFoundException {
        return get(GetIdByNameResult.class, HttpParameters.create()
                .parameter("type", "id")
                .parameter("name", playerName))

                .get()
                .executeParsing();
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public GetNameByIdResult getPlayerName(int playerId) throws PlazmixNotFoundException {
        return get(GetNameByIdResult.class, HttpParameters.create()
                .parameter("type", "name")
                .parameter("id", playerId))

                .get()
                .executeParsing();
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public GetPlayerInfoResult getPlayerInfo(int playerId) throws PlazmixNotFoundException {
        return get(GetPlayerInfoResult.class, HttpParameters.create()
                .parameter("type", "id_info")
                .parameter("id", playerId))

                .get()
                .executeParsing();
    }

    @SneakyThrows({ExecutionException.class, InterruptedException.class})
    public GetPlayerInfoResult getPlayerInfo(@NonNull String playerName) throws PlazmixNotFoundException {
        return get(GetPlayerInfoResult.class, HttpParameters.create()
                .parameter("type", "name_info")
                .parameter("name", playerName))

                .get()
                .executeParsing();
    }


    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class GetIdByNameResult extends PlazmixHttpResponse<GetIdByNameResult> {

        int id;

        public GetIdByNameResult(@NonNull int responseCode, @NonNull Class<GetIdByNameResult> requestType, @NonNull String responseBody) {
            super(responseCode, requestType, responseBody);
        }
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class GetNameByIdResult extends PlazmixHttpResponse<GetNameByIdResult> {

        String name;

        public GetNameByIdResult(@NonNull int responseCode, @NonNull Class<GetNameByIdResult> requestType, @NonNull String responseBody) {
            super(responseCode, requestType, responseBody);
        }
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class GetPlayerInfoResult extends PlazmixHttpResponse<GetPlayerInfoResult> {

        int id;

        String name;

        GroupResult group;
        BalanceResult balance;
        LevelingResult leveling;

        public GetPlayerInfoResult(@NonNull int responseCode, @NonNull Class<GetPlayerInfoResult> requestType, @NonNull String responseBody) {
            super(responseCode, requestType, responseBody);
        }

        @Getter
        @FieldDefaults(level = AccessLevel.PRIVATE)
        public static class GroupResult {

            int level;

            String name;
            String displayName;

            String prefix;
            String suffix;

            String tagPriority;
        }

        @Getter
        @FieldDefaults(level = AccessLevel.PRIVATE)
        public static class LevelingResult {

            int level;
            int experience;
            int maxExperience;
        }

        @Getter
        @FieldDefaults(level = AccessLevel.PRIVATE)
        public static class BalanceResult {

            int coins;
            int plazma;
        }
    }
}
