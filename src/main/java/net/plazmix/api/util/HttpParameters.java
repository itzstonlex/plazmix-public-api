package net.plazmix.api.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public final class HttpParameters {

    public static HttpParameters create() {
        return new HttpParameters();
    }

    @NonNull
    Map<String, String> parameters = new HashMap<>();

    public HttpParameters parameter(@NonNull String key, @NonNull Object value) {
        parameters.put(key, value.toString());
        return this;
    }

    @SneakyThrows
    private String encode(@NonNull String value) {
        return URLEncoder.encode(value, "UTF-8");
    }

    public String apply(@NonNull String baseUrl) {
        return (baseUrl + (baseUrl.contains("?") ? "" : "?")) + parameters.entrySet()
                .stream()

                .map(entry -> entry.getKey() + "=" + (entry.getValue() != null ? encode(entry.getValue()) : "null"))
                .collect(Collectors.joining("&"));
    }

}
